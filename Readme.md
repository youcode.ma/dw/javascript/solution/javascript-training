# Bases JavaScript

# Partie 1: Variables

1. Créez une variable contenant votre citation préferée, et l'affichez dans la console.  

2. Ecrivez un programme JavaScript pour trouver l'aire d'un triangle dont les longueurs sont égales à 5, 6, 7.  
Heron Formula: https://www.mathwarehouse.com/geometry/triangles/area/herons-formula-triangle-area.php  

# Partie 2: Conditions  

3. Ecrivez un programme JavaScript qui détermine si un entier N est impair ou pair.  

4. Ecrivez un programme JavaScript pour déterminer si une année donnée est une année bissextile dans le calendrier grégorien.  

5. Ecrivez un programme JavaScript qui prend un entier aléatoire compris entre 1 et 10, puis l’utilisateur est  invité à saisir un nombre approximatif. Si l'entrée utilisateur correspond au numéro approximatif, le programme affichera le message "Bon travail", sinon le message "Ne correspond pas".  

6. Ecrivez un programme JavaScript pour créer une nouvelle chaîne de caractères en ajoutant "You" devant la chaîne de caractères donnée. Si la chaîne donnée commence par "You", retournez la chaîne de caractères d'origine.
P.S: vous pouvez utiliser la méthode substring()

7. Ecrivez un programme JavaScipt qui affiche la mention selon la note obtenu. En utilisant Switch.  

# Partie 3: Boucles 

8. Ecrire un programme JavaScript qui permet de calculer le produit de deux entiers en utilisant des additions successives.  

9. Ecrire un programme JavaScript permettant de lire un nombre entier N puis calcule son factoriel.  
Utilisez for,  
Utilisez while.  

10. Ecrivez un programme JavaScript pour compter le nombre de voyelles dans une chaîne de caractères donnée.  

# Partie 4: Fonctions

11. Créez une fonction appelée buildTriangle() qui prend comme paramètre ( la largeur maximale du triangle) et renvoi la représentation sous forme de chaîne d'un triangle.  
Voir l'exemple de sortie ci-dessous.  
```
buildTriangle(6);
résultat affiché:
* 
* * 
* * * 
* * * * 
* * * * * 
* * * * * * 
```

12. Ecrivez une fonction anonyme en stockant la fonction dans une variable appelée "rire" et affiche le nombre de "ha" que vous passez en tant qu'argument.  
```
rire(4);  
résultat: hahahaha  
```

13. Ecrivez une expression de fonction non anonyme qui stock la fonction dans une variable appelée "crier" et retourne "aaa!".  

14. Fonctions inline:  
la fonction emotion est définit comme suit:  
```
function emotions(msgString, nomFonction) {
    console.log("Je suis " + msgString + ", " + nomFonction(2));
}
```
Appelez la fonction emotions() pour qu'il affiche le resultat ci-dessous, mais au lieu de faire appelle à la fonction rire comme argument, vous placez l'expression de la fonction rire.  
```
emotions("heureux", rire(2));  
résultat: "Je suis heureux, haha!"  
```

# Partie 5: Tableaux

15. Considérons le tableau suivant.
var actorsInFilm = ["Tom Hanks", "Jim Parson", "Johnny Depp", "Anthony Hopkins", "Anne Hathaway", "Rami Malek"];

Créez une fonction appelée isGoodMovie () qui prend le tableau actorsInFilm comme argument et renvoie true si le tableau contient votre acteur préferé.

16. Ecrivez un programme JavaScript pour permuter le premier et le dernier élément d'un tableau d'entiers. Le tableau doit contenir au moins un element.

17. Ecrivez un programme JavaScript pour trier un tableau de chaine de caracteres dans l'ordre croissant.

18. Utilisez la méthode map () pour prendre le tableau prixHT présenté ci-dessous et créer un second tableau prixTotal qui affiche le prix avec 20% de tva ajouté.
```
var prixHT = [50.23, 19.12, 34.01, 100.11, 12.15, 9.90, 29.11, 12.99, 10.00, 99.22, 102.20, 100.10, 6.77, 2.22];
```
Afficher le nouveau tableau dans la console.

19. Utilisez une boucle for imbriquée pour balayer les element du tableau ci-dessous et remplacer tous les nombres pairs par la chaîne "paire" et tous les autres nombres par la chaîne "impaire".

```
var numbers = [
    [243, 12, 23, 12, 45, 45, 78, 66, 223, 3],
    [34, 2, 1, 553, 23, 4, 66, 23, 4, 55],
    [67, 56, 45, 553, 44, 55, 5, 428, 452, 3],
    [12, 31, 55, 445, 79, 44, 674, 224, 4, 21],
    [4, 2, 3, 52, 13, 51, 44, 1, 67, 5],
    [5, 65, 4, 5, 5, 6, 5, 43, 23, 4424],
    [74, 532, 6, 7, 35, 17, 89, 43, 43, 66],
    [53, 6, 89, 10, 23, 52, 111, 44, 109, 80],
    [67, 6, 53, 537, 2, 168, 16, 2, 1, 8],
    [76, 7, 9, 6, 3, 73, 77, 100, 56, 100]
];
```

# Partie 6: Objets

20. Créez un objet door qui a comme propriété color (string), isOpen (booléan) et deux fonctions open et close qui changent l'état de isOpen.

21. Ecrivez un programme JavaScript pour afficher les propriétés d'un objet JavaScript. 

```
var student = { 
name : "Asmaa", 
class : "Namek", 
cursus : "Web dev" };
résultat: name,class,cursus
```

22. Créez un objet qui contient 28 ou 27 apprenants d'une classe et génère 5 objets pour presenter 5 groupes de 5 ou 6 apprenants avec le responsable de chaque groupe d'une manière aléatoire. et determine le groupe chanceux de la journée parmi les 5 groupes.