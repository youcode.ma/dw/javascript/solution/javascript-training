var tableau = ["abcd", "abd", "a", "abcdef", "z" , "f", "raqi ismail"];

function trier(tab) {
    var temp;
    var i, j;

    for (i = 0; i < tab.length; i++)
        for (j = 0; j < tab.length; j++)
            if (tab[i].length < tab[j].length) {
                temp = tab[i];
                tab[i] = tab[j];
                tab[j] = temp;
            }
}

trier(tableau);
console.log(tableau);