var namek = {
    students: ["Lahcen Aboulala", "Hamza Boussif", "Yaacoub Abouayad", "Ayoub El marzak", "Youssef Baghour", "Ismail Raqi", "Imad Fathallah", "Alae Es-saki", "Hisham Ezzyty", "Ayoub Ajrhourh", "Khalil Kessa", "Ayoub Bengara", "Iliass Bougaze", "Ibtissam Belamria","Elgouail Rabia", "Mehdi Bellafdil", "Fayçal Draissi", "Omar Elaibi", "Nachib soukaina", "karima Bengara", "Yassine Lafkih", "Leila Mhaidrat", "Rahma Omara", "Saba Larif", "Houria Chafiq", "Imane Elkasmi"],
    studentsOld: ["zoloukolo", "omar elaibi", "youssef baghour", "boussif hamza", "mehdi chennak", "meryeme eddoug", "sarhabil othmane", "ayoub elmarzak", "khalid esserhir", "karima bengara", "lahcen aboulala", "youssef allam", "nazih chaimaa", "balibla fouzia", "yaacoub abouayad", "loutfallah aguerouaz", "asmaa elhouari", "hicham ezzyty", "abdellah zarraq", "khaoula bendaoud", "raqi ismail", "ayoub ajrhourh", "nadi fatima ezzahra", "zineb bouhmane", "hanane bellagh", "nouhaila iounousse", "imad fathallah"],
    groups: [],
    shuffle: function () {
        var i, j, x;
        for (i = this.students.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = this.students[i];
            this.students[i] = this.students[j];
            this.students[j] = x;
        }
    },
    generateGroups: function (size) {
        var i;
        this.groups = [];

        this.shuffle();

        for (i = 0; i < this.students.length; i += size) {
            var group = this.students.slice(i, i + size);
            console.log(group);
            this.groups.push(group);
        }
        if((this.students.length % size) != 0) {
            for(i = 0; i < (this.students.length % size); i++) {
                this.groups[i].push(this.groups[this.groups.length - 1][i]);
            }
            this.groups.pop(); 
        }
    }
}

function populateTable(array) {
    var table = document.getElementById("groups-table"),
        i;
    for (i = 1; i < table.rows.length; i++) {
        for (var j = 1; j < table.rows[i].cells.length; j++) {
            table.rows[i].cells[j].innerHTML = array[i - 1][j - 1];
        }
    }
}

function shuffling() {
    namek.shuffle();
    console.log(namek.students);
}

function generating() {
    namek.generateGroups(5);
    console.clear();
    console.log(namek.groups);
    populateTable(namek.groups);
    document.getElementById("tb").style.display = "block";
    document.getElementById("bt").innerHTML = "Regenerate Groups";

}

var tm;

function start() {
    tm = window.setInterval('generating()', 100);
    document.getElementById("stop").style.display = "block";
    document.getElementById("bt").style.display = "none";
}

function stop() {
    window.clearInterval(tm);
    document.getElementById("bt").style.display = "block";
    document.getElementById("stop").style.display = "none";
}